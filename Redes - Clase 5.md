Institution: [[UBA]]
Course: [[Teoría de las Comunicaciones]]
Type: #source #lecture
Topics: #networking

---

# Sistemas Autónomos

**[[Internet]]** se organiza como un **[[Grafo|grafo]] jerárquico** de **sistemas autónomos** (AS) que se conectan entre sí. Éstos sistemas autónomos están conformados por conjuntos de routers, que a su vez se conectan con una red de hosts. Además, cuentan con un **administrador** (en internet son los **ISPs**) que se encarga de mantener el sistema.
* Cuando un host de un AS desea comunicarse con uno que esté en otro AS, debe pasar por un **Border Router**, que maneja el tráfico con los AS externos.
* Los detalles internos de cada AS están ocultos ante los demás. De esta forma, sistemas **heterogéneos**, que utilizan distintos protocolos/tecnologías, se pueden comunicar entre sí.

# Protocolos de Ruteo

Los protocolos pueden ser de:
* Ruteo Interno: conectan a las redes dentro de un mismo AS. **TODO**
* Ruteo Externo: conectan a los distintos AS entre sí. **TODO**

El **ruteo** es un problema de **grafos**, **optimización** y **algoritmos distribuidos**. Los hosts de la red se representan como nodos en un grafo, y sus conexiones como arcos con pesos que indican "costo" (en general en **tiempo**) de utilizar ese enlace.

**Rutear** dos paquetes implica el **[[Shortest Path|camino de menor costo]]** entre dos nodos, en tiempos razonables, usando **recursos mínimos**. Se puede separar en:
* **Protocolos Estáticos**: Configuración **manual**.
* **Protocolos Dinámicos**: Configuración **autónoma** y **adaptiva** (a medida que fallan nodos/se agregan nuevos).

## Routing vs. Forwarding

Es importante hacer una distinción entre:
* **Re-envío (forwarding)**: proceso para seleccionar una **puerta de salida** basado en la dirección de destino y las **tablas de forwarding**.
* **Ruteo**: proceso mediante el cual son construidas y actualizadas las **tablas de ruteo**.

## Protocolos Intradominio

Primero, vamos a ver los protocolos que se utilizan dentro de un AS para comunicar a sus nodos entre sí. Están diseñados para encontrar el camino de menor costo entre aquellos que se quieren comunicar. Hay 2 tipos generales:
* **Distance-Vector**:
	* Cada router informa, sólo a **sus vecinos**, **toda su tabla de ruteo**.
	* El algoritmo es un **[[Algoritmo de Bellman-Ford|Bellman-Ford]] distribuido**.
* **Link-State**:
	* Cada router informa, a **toda la red**, el estado de sus **enlaces directos**.
	* El algoritmo es **[[Dijkstra]]**.

### Vector de Distancia

* Cada nodo mantiene una tabla hacia **todos los nodos** con los campos $\langle Destination, Cost, NextHop \rangle$.
* Intercambia actualizaciones sus vecinos **directamente conectados**:
	* Periódicamente (cada varios segundos).
	* Cuando la tabla cambia.
* Las actualizaciones contienen todos los pares $\langle Destination, Cost \rangle$.
* La tabla de un nodo se actualiza cuando se encuentra un camino con costo menor para un $Destination$.

**[[RIP]]** es el protocolo de vector de distancias más utilizado.

Estos protocolos son susceptible al problema del **conteo a infinito**:
* El enlace $A$ a $E$ **falla**.
* $A$ comunica a $B$ y $C$ distancia $\infty$ a $E$.
* $B$ y $C$ comunican distancia $2$ a $E$ (esto requiere que escuche el mensaje de $C$ **antes que el de** $A$.
* $B$ decide que, gracias a $C$, puede llegar a $E$ en $3$ hops, y se lo cuenta a $A$.
* $A$ decide que, gracias a $B$, puede llegar a $E$ en $4$ hops, y se lo cuenta a $C$.
* $C$ decide que, gracias a $A$, puede llegar a $E$ en $5$ hops, y se lo cuenta a $B$.
* Este ciclo se repite *ad infinitum*.

**TODO**: Imagen

Para romper estos ciclos, se pueden utilizar ciertas heurísticas:
* **Fijar** un número como infinito (RIP usa esto, el máximo es 16).
	* Esto sólo se puede usar para redes de tamaño pequeño.
* **Partir el horizonte**: En el envío de actualizaciones, **omitimos** la información que acabamos de aprender por parte de otro nodo.
* Partir el horizonte con **reverso venenoso**: sí notificamos las entradas aprendidas desde el nodo, pero a esos destinos les pone **costo infinito**.
* Las últimas 2 sólo evitan problemas con **ciclos de tamaño 2**.

La convergencia del protocolo no es muy buena. Para mejorar esto, se pueden utilizar protocolos de **Estado de Enlace**.

### Estado del enlace

* En estos protocolos, cada nodo **inunda** la red con paquetes que contienen información de sus **enlaces directos**. Los paquetes, denominados **LSP** (Link State Packet), están compuestos por:
	* **ID** del router que creó el LSP.
	* **Costo** a cada vecino.
	* **SEQNO**, el número de secuencia, que los nodos incrementan en cada LSP enviado.
	* **TTL** del paquete (para que los paquetes no circulen infinitamente, y congestionen demasiado la red).
* Los routers:
	* **Almacenan** el LSP más reciente de cada nodo (el de mayor SEQNO)
	* **Decrementan** el TTL de los LSP recibidos.
		* Si es $>0$, lo **forwardean** a los demás.
		* Si no, lo **descartan**.
	* Progresivamente, construyen un subgrafo de nodos hacia los cuáles conocen el camino mínimo, de forma análoga al **[[Algoritmo de Dijkstra]]**.
* En **OSPF**, se tiene inundación **confiable**, mediante **ACK** a los LSPs.
	* 

El problema de este protocolo es la carga en los routers: cada uno debe almacenar un LSP para cada nodo en la red.

#### OSPF jerárquico

En la práctica, **RIP** se utiliza para redes de bajo tamaño y tráfico, mientras que OSPF se utilizar para que las distintas AS se comuniquen entre sí. 

Cuando se organiza la red como un árbol, donde tenemos **routers troncales** que conectan a los **routers de frontera de área**, y se organizan en una jerarquía. Esto mejora la **escalabilidad** a cambio de la optimalidad de las decisiones.

## Protocolos Interdominio

Los **protocolos interdominio** (como) son los que utilizan los **routers de frontera** para comunicarse entre sí. Éstos sólo transmiten información sobre las redes que tienen dentro de su sistema autónomo.

**TODO: EGP y BGP**