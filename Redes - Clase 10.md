Institution: [[UBA]]
Course: [[Teoría de las Comunicaciones]]
Type: #source #lecture
Topics: #networking

---

# Seguridad

## Conceptos

Existen distintas propiedades englobadas por la idea general de seguridad:

* **Confidencialidad**: La información **sólo debe ser accesible** por las partes especificadas de la comunicación.
* **Integridad**: Los mensajes enviados **no pueden ser modificados** durante la transmisión.
	* **Originalidad**: El mensaje recibido no es una **copia artificial** del mensaje original.
	* **Temporalidad**: El mensaje **no fue demorado** maliciosamente.
* **Autenticación**: Ninguna parte puede asumir en forma no autorizada **la identidad** de otra parte.
* **Autorización/Control de Acceso**: Sólo ciertos usuarios pueden realizar ciertas acciones permitidas.
* **Disponibilidad**: Todo usuario tendrá la oportunidad de ser **considerado** y **eventualmente admitido**.
* **No repudiación**: Ninguna de las partes puede **negar haber participado**.

## Seguridad en Capas

Al igual que otros servicios, como la confiabilidad, los distintos conceptos de la seguridad pueden ser proveídos por distintas capas del **[[Modelo OSI|modelo OSI]]**.

**TODO**: Imagen

## Criptografía

El cifrado es una **transformación** a nivel de caracter/bit sobre el mensaje, ignorando su estructura lingüística.

### Código

Cuando utilizamos un **código**, reemplazamos cada palabra o símbolo por otro, dada por un conjunto de reglas (no necesariamente tiene que ser un mapeo uno a uno).

### Conceptos de Cifrado

* Los mensajes a ser encriptados se denominan "**plaintext**".
* Éstos son tomados como una **entrada** y transformados por una **función de encripción**, que está parametrizada por una **clave**.
* La **salida** de la función es denominada "**ciphertext**".
* Se asume que un **intruso** puede **escuchar** y **copiar** el ciphertext **completo**.

**TODO**: Imagen

### Métodos Básicos

* **Sustitución**: Cada **letra/grupo de letras** se reemplaza por otra letra/grupo de letras. Se preserva el **orden** de las letras/grupos.
	* **Ejemplos**: Cifrado de césar, sustitución monoalfabética.
	* En general, es fácil romper estos cifrados si se tienen en cuenta las propiedades estadísticas de los lenguajes naturales.
* **Transposición**: Se **reordenan** las letras, sin reemplazarlas.

### Principio de Kerckhoffs

El **principio de Kerckhoffs** establece que:

> Los **algoritmos de encripción** deben ser **públicos**, sólo las **claves** deben ser **secretas**.

### Longitud de Clave

Como el **secreto** de un algoritmo de encripción **reside en la clave**, su **longitud** es un factor de diseño crítico.
* A mayor longitud de clave, mayor **factor de trabajo**, es decir, tiempo requerido por parte del criptoanalista para romper la clave.
	* El **factor de trabajo** crece exponencialmente con el tamaño de clave.

### One-Time Pads

Este es un tipo de cifrado **inviolable**.
* Se basa en elegir un **random bit string** como clave (en general con la **misma longitud que el plaintext**).
* Para encriptar/desencriptar, se hace un **XOR** entre la clave y el plaintext.
* Según Shannon, es inmune a todos los ataques actuales y futuros, sin importar cuánta potencia computacional tenga el intruso.
* Es **impráctico**, por el problema de distribuir la clave de forma segura.
* Se utiliza como **punto de referencia** para otros algoritmos

### Cifrado Simétrico/Clave Privada

En las técnicas de cifrado simétrico, tenemos funciones $E_K$, $D_K$ (parametrizada por $K$) donde $E_K$ es la **función de encripción** y $D_K$, la **función de desencripción**, es su [[Función Inversa|inversa]]. $K$ es la clave privada, que debe ser conocida por ambas partes de la comunicación.

#### Cifrado de Bloque Iterativo

En estos códigos, se combinan una serie de **transformaciones** y **permutaciones** sobre bloques de $n$ bits, utilizando como parámetro la clave.

**TODO**: Imagen

##### DES

**DES** (Data Encryption Standard) es uno de los primeros **cifrados por bloques** (en realidad trabaja a nivel bits) con uso masivo.
* Con el tiempo fue quedando obsoleto ante ataques de **fuerza bruta**.
* También se encontraron vulnerabilidades para algunas **claves específicas**.

**TODO:** Imagen

### Cifrado Asimétrico/Clave Pública

La principal **desventaja** de los algoritmos previos es la necesidad de **distribuir** la **clave privada** por un **canal seguro**. Los **cifrados asimétricos** solucionan esto partiendo cada clave en una parte **pública** y una **privada**.

Estos algoritmos cuentan con dos funciones $K^-_B$ y $K^+_B$, donde la primera es la privada y la segunda la pública.
* El cifrado de $m$ es $K^-_B(m)$, y se puede desencriptar usando $K^+_B$.
* Si alguien desea enviarme un mensaje $m$, puede utilizar $K^+_B(m)$, y yo recupero $m$ usando $K^-_B(m)$.

Es decir, las funciones son **[[Función Inversa|inversas]]** entre sí:
$$m = K^+_B(K^-_B(m)) = K^-_B(K^+_B(m))$$

#### Encripción RSA

**TODO**

## Firma Digital

**TODO**

## Autenticación

**TODO**
