Institution: [[UBA]]
Course: [[Teoría de las Comunicaciones]]
Type: #source #lecture
Topics: #networking

---

# Nivel de Transporte

El **nivel de transporte** es la cuarta capa del [[Modelo OSI]], y su unidad de transporte es el **TPDU** (transport protocol data unit). Al igual que el resto de las capas, utiliza servicios de la capa inferior (la de red) y le provee servicios a las superiores. El principal servicio que brinda es el de **confiabilidad** (aunque algunos protocolos, como **[[UDP]]** no lo tienen).

Un servicio de **Transporte**:

* Potencialmente **conecta muchas máquinas diferentes**.
	* Requiere establecimiento y finalización de conexión explícitos.
* Potencialmente lidia con **diferentes RTT**.
	* Requiere mecanismos adaptativos para **timeouts**.
* Potencialmente enfrenta **largos retardos en la red**.
	* Requiere estar preparado para el arribo de paquetes **muy antiguos**.
* Potencialmente existe **diferente capacidad de recepción de destino**.
	* Requiere contemplar nodos de diferentes capacidades.
* Potencialmente maneja **diferentes capacidades de red**.
	* Requiere **TODO**.

## Máquina de estados general

Para un **servidor**, las transiciones de estados de la capa 4 son las siguientes:

```mermaid
stateDiagram-v2
	state "EN REPOSO" as reposo
	state "ESTABLECIMIENTO PASIVO" as establecimientoPendiente
	state "ESTABLECIDA" as establecida
	state "DESCONEXIÓN PASIVA PENDIENTE" as desconexionPendiente

	reposo --> establecimientoPendiente: TPDU de solicitud de conexión recibida
	establecimientoPendiente --> establecida: Primitiva de conexión ejecutada
	establecida --> desconexionPendiente: TPDU de solicitud de desconexión recibida
	desconexionPendiente --> reposo: Primitiva de desconexión ejecutada
```

Por otro lado, para un cliente, el diagrama sería el siguiente:

```mermaid
stateDiagram-v2
	state "EN REPOSO" as reposo
	state "ESTABLECIMIENTO ACTIVO PENDIENTE" as establecimientoPendiente
	state "ESTABLECIDA" as establecida
	state "DESCONEXIÓN ACTIVA PENDIENTE" as desconexionPendiente

	reposo --> establecimientoPendiente: Primitiva de conexión ejecutada
	establecimientoPendiente --> establecida: TPDU de conexión aceptada recibida
	establecida --> desconexionPendiente: Primitiva de desconexión ejecutada
	desconexionPendiente --> reposo: TPDU de solicitud de desconexión recibida
```

*En realidad, como vemos más adelante, cualquiera de las partes puede iniciar una desconexión, y no necesita esperar al TPDU para liberarla (esto es necesario para evitar ataques [[DDoS]])*

## Protocolos End-to-End

Los protocolos End-to-End se apoyan en la capa de red (que es de mejor esfuerzo). Buscamos que ofrezcan las siguientes propiedades: para
* Garantizar la (eventual) entrega de mensajes (**confiabilidad**).
* Entrega de mensajes en el mismo **orden** en el que fueron enviados.
* Entrega de a lo sumo una copia de cada mensaje.
* Soporte para mensajes **arbitrariamente largos**.
* Soporte de **sincronización**.
* Permitir al receptor controlar el **flujo de datos** del transmisor.
* Soportar **múltiples procesos** de nivel de aplicación que conecten al **mismo par** de emisor/receptor.

### [[TCP]]

**TCP** es un protocolo de capa 4. Tiene las siguientes propiedades:

* Es **orientado a la conexión**.
	* Tiene un **3-way handshake** para el **establecimiento** de la conexión.
	* Tiene un **2-2-way handshake**p para la **liberación** de la conexión.
* Se basa en el **flujo de bytes**:
	* La aplicación emisora escribe bytes en un buffer de envío.
	* TCP envía **segmentos**.
		* El protocolo espera (hasta un timeout) a que el buffer tenga suficientes bytes para enviar un segmento grande.
		* Los segmentos tienen un **MSS** (maximum segment size), que por default es 536 bytes.
	* La aplicación receptora lee bytes de un buffer de llegada.
* Es **confiable**.
	* Usa **checksum** para garantizar la integridad de los mensajes.
	* Utiliza [[Redes - Clase 2#Sliding Window|ACK acumulativo]]. Se secuencia **cada byte** de la transmisión.
	* Los datos corruptos/perdidos/fuera de orden no son acknowledged, así que su emisor los **retransmite** después de un **timeout**.
* Es **full duplex** (hay comunicación para los dos lados simultáneamente). Para lograrlo, se tienen 2 controles:
	* Control de **flujo**: evita que el transmisor inunde el buffer del receptor.
	* Control de **congestión**: evita que el transmisor sobrecargue la red.

![[tcp-connection-buffering.png]]

#### Establecimiento y Liberación

![[tcp-start-finish-sequence.png]]

* El **establecimiento** de las conexiones TCP se da por medio de un **3-way handshake**:
	* Primero, el **cliente** envía un segmento con el flag **SYN**, indicando que **desea establecer** una conexión.
	* Luego, el **servidor** responde con un segmento que reconoce el anterior **ACK**, y también tiene el flag **SYN**, indicando que está **dispuesto a participar** en una conexión.
	* Finalmente, el cliente reconoce esto por medio de un segmento con el flag **ACK**.
* La **liberación** de las conexiones TCP sigue un **(2-2/4)-way handshake**:
	* **TODO**

La máquina de estados que define estos procesos se puede ver ilustrada a continuación, donde cada transición es de la forma *Evento/Acción*: el evento indica qué disparó la transición (puede ser mensaje o una primitiva), y la acción indica qué se hace al pasar al siguiente estado.

![[tcp-state-machine.png]]

#### Puertos

Cada conexión es identificada por la 4-upla:
$$\langle IP_{src}, Port_{src}, IP_{dst}, Port_{dst} \rangle$$
Los **puertos** identifican al programa de **nivel aplicación** que está participando en la conexión. Como el campo correspondiente en el header de TCP tiene **16 bits**, hay $2^{16} = 65536$ puertos posibles.

Podría utilizarse el **PID del proceso** en lugar del puerto, pero esto traería problemas:
* Los procesos que se forkean perderían acceso a la conexión, porque el PID del proceso hijo cambia.
* Se volvería imposible la práctica de utilizar **well-known ports** para ciertos **protocolos de nivel aplicación**, ya que en Linux los PID se asignan automáticamente.

#### Primitivas

**TCP** con una serie de primitivas que pueden utilizar las capas superiores. Las de los **sockets Berkeley TCP** (las más prevalentes) son:

* Servidor:
	* **Socket**: Crea un **nuevo punto terminal de comunicación** y asigna espacio en las tablas de la entidad de transporte.
	* **Bind**: **Adjunta un puerto** a un socket dado, evitando que otros procesos utilicen ese puerto.
	* **Listen**: Anuncia la **disposición de aceptar conexiones**. En esta instrucción se indica el **tamaño de la cola** de clientes sin atender.
	* **Accept**: **Bloquea** al proceso invocador hasta que **llegue una conexión** en el socket dado.
* Cliente:
	* **Connect**: Intenta **establecer activamente** una conexión.
* Ambos:
	* **Send**: **Envía datos** a través de la conexión.
	* **Receive**: **Recibe** datos de la conexión.
	* **Close**: **Libera** la conexión.

![[tcp-sockets.png]]

#### Confiabilidad

Para brindar la confiabilidad, utiliza una serie de técnicas:

**TODO**

#### Formato del segmento

![[tcp-header.png]]

Campos:
* **Puerto fuente y destino (16 bits)**: Identifican los puntos finales locales de la conexión.
* **Número de secuencia (32 bits)**: Identifica de forma unívoca los datos de aplicación que contiene el segmento **TCP**. Identifica el **primer byte de los datos que contiene**.
* **Número de reconocimiento (ACK) (32 bits)**: Indica el siguiente byte **que espera el receptor**.
	* Implica que todos los bytes hasta ese fueron **recibidos correctamente**.
* **Longitud de cabecera (4 bits)**: Indica cuántas words de 32 bits mide el header.
* **Flags**: Los importantes son:
	* **URG**: es 1 cuando el campo de **Puntero a Urgente** está en uso. Este puntero indica hasta dónde los datos son considerados urgentes
	* **ACK**: es 1 si el segmento indica que se reconocieron datos previos (si es 0 el número de reconocimiento es ignorado).
	* **PSH**: indica que el receptor debe entregar los datos a la aplicación inmediatamente, en vez de esperar a que se llene el buffer.
	* **RST**: se utiliza para resetear una conexión que se ha vuelto confusa (por ejemplo, debido a la caída de un host).
	* **SYN**: Es utilizado para **establecer** conexiones.
	* **FIN**: Es utilizado para **liberar** conexiones.
* **Tamaño de la ventana**: Indica cuántos bytes pueden ser enviados desde el último reconocido. Se utiliza para el **control de flujo**.

#### Control de Flujo

##### Stream de Bytes

Un segmento del stream se envía cuando:

* El buffer llega al MSS (segmento full).
* No está full, pero transcurre un tiempo de timeout.
* La aplicación usa la primitiva **PUSH**.

##### Ventana deslizante

* El recurso escaso en las transmisiones TCP es el **espacio de buffer**: se suelen asignar de $4KB$ a $8KB$.
* En el buffer del emisor, se tienen que mantener:
	* Los bytes que está por enviar.
	* Los bytes que están "en vuelo", es decir, que se enviaron pero todavía no se recibió el **ACK**. Esto es por si hay algún error y necesitan ser retransmitidos.
* La política de retransmisión es **[[Redes - Clase 2#Sliding Window|Go Back N]]**.
* El tamaño de la ventana es indicado por el receptor.

![[tcp-sliding-window.png]]

###### Ventana deslizante: Emisor

* Del lado **emisor**, se tienen tres punteros:
	* $LastByteACKed$ indica el último byte que el receptor **recibió correctamente**.
	* $LastByteSent$ indica el último byte "**en vuelo**".
	* $LastByteWritten$ indica el último byte que la **aplicación escribió** al buffer.
* Deben mantener las siguientes relaciones:
$$
\begin{aligned}
LastByteACKed & \leq LastByteSent \\
LastByteSent & \leq LastByteWritten
\end{aligned}
$$
* Por ende, se bufferean los bytes entre $LastByteACKed$ (los que están antes fueron confirmados) y $LastByteWritten$ (los que están después no fueron generados).

![[tcp-sliding-window-sender.png]]

###### Ventana deslizante: Receptor

Las relaciones son menos intuitivas debido al reodenamiento:
* El siguiente byte esperado es el siguiente en el stream consecutivo de bytes recibidos: Los bytes leídos por la aplicación deben ser anteriores:
$$LastByteRead < NextByteExpected$$
* Un byte no puede ser **leído por la aplicación** antes de que éste **y todos sus precendentes** se hayan recibido:
$$NextByteExpected \leq LastByteRecv + 1$$
* Si los bytes llegan en orden, se cumple la igualdad.
* Se deben bufferear todos los bytes recibidos entre $NextByteExpected$ y $LastByteRecv$.

![[tcp-sliding-window-receiver.png]]

##### Tamaños de Buffers

Por el lado del **receptor**, se tiene:
* $(LastByteRcvd - LastByteRead) \leq MaxRcvBuffer$
* $AdvertisedWindow := MaxRcvBuffer - (LastByteRcvd - LastByteRead)$
Mientras tanto, para el **emisor**:
* $(LastByteSent - LastByteACKed) \leq AdvertisedWindow$
* $EffectiveWindow := AdvertisedWindow - (LastByteSent - LastByteACKed)$
* $LastByteWritten - LastByteACKed \leq MaxSendBuffer$

Por ender, se debe:
* **Bloquear la transmisión** si se desean escribir $x$ bytes y $(LastByteWritten - LastByteACKed) + x > MaxSendBuffer$.
* **Enviar desde el receptor** los ACK en respuesta a la llegada de segmentos con datos consecutivos.
* **Persistir desde la transmisión** enviando $1$ byte cuando $AdvertisedWindow = 0$.

![[tcp-sliding-window-ejemplo-secuencia.png]]

#### Extensiones de TCP

Son implementadas como opciones del encabezado, y pueden mejorar la eficiencia del protocolo. Se pueden:

* Almacenar marcas de tiempo en segmentos de salida.
* **TODO**

#### Retransmisión Adaptativa

El algoritmo original de **retransmisión adaptativa** es:

* Se mide un $SampleRTT_i$ para cada par (segmento, ACK).
* Actualiza el valor de la siguiente manera (es un filtro pasa-baja):
$$EstimatedRTT_{i} = \alpha \cdot EstimatedRTT_{i-1} + \beta SampleRTT_i$$
Se suelen tomar **TODO**

* El timeout se toma como $TimeOut = 2 \cdot EstimatedRTT$

## Servicios sin conexión

### UDP

**TODO**