Institution: [[UBA]]
Course: [[Teoría de las Comunicaciones]]
Type: #source #lecture
Topics: #networking

---

# Congestión

Los **buffers** de una red tienen una tendencia a llenarse.
* Se denomina **buffering** al efecto de tener los paquetes en la cola de un buffer. Reduce pérdidas, pero incrementa el **delay**.
* Cuando se llena el buffer de un router, se tiene un **buffer overflow**, y se empiezan a descartar paquetes. Esto produce retransmisiones y reduce el QoS.

La **congestión** es un estado de sobrecarga sostenida de una red.
* La demanda de recursos (enlaces y buffers) se encuentra al límite o excede la capacidad existente.
* La consecuencia es un QoS degradado.
* Soluciones posibles
	* Sobredimensionamiento (overprovisioning).
	* Diseño cuidadoso para casos específicos.
	* Control **proactivo**/**preventivo** (en vez de **reactivo**): Evitar la congestión.
* Se puede ver de distintas perspectivas:
	* Para un **usuario**, la red está congestionada cuando el QoS percibido por el usuario decrece.
	* Para un **operador de red**, un **puerto CMTS** (Cable Modem Termination System) está en **Near Congestion State** cuando su tráfico entrante/saliente:
		* Excede un nivel específico (el **Port Utilization Threshold**).
		* Durante un tiempo determinado (el **Port Utilization Duration**).

## [[Teoría de Colas]]

Para modelar la congestión, consideramos un sistema cola-servidor en el cual el número de llegadas es un **[[Proceso Markoviano|proceso markoviano]]** (de [[Distribución de Poisson|poisson]]) y el tiempo de servicio también (es uno [[Distribución Exponencial|exponencial]]). Hay un único servidor, y la cola tiene capacidad infinita.

**TODO: Imagen**

* $N$ es la cantidad total de paquetes en el sistema.
* $T$ es el tiempo de estadía de cada paquete en el sistema.
* $\lambda$ es la **tasa de entrada** de paquetes a procesar (paquetes/seg).
	* $\frac{1}{\lambda}$ es el tiempo medio entre llegadas.
* $\mu$ es la **tasa de servicio** de paquetes procesados (paquetes/seg).
	* $\frac{1}{\mu}$ es el tiempo medio de servicio.
* $\rho = \frac{\lambda}{\mu}$ es la intensidad del sistema.
	* $\rho < 1$ si y sólo si no hay congestión.

**TODO**: Terminar esto

También se tiene que:

* $E[N] = \frac{\rho}{1 - \rho}$
* $E[N] = \lambda E[T]$.
* $E[T] = \frac{1}{\mu - \lambda}$

## Control de Congestión - Red

El control de congestión de la red ataca un problema diferente al control de flujo por parte del receptor.

Se asume que los routers actúan como "**Drop Tail**" (FIFO), es decir:
* Los paquetes se transmiten en el orden que llegan.
* Cuando se descartan paquetes se descartan los últimos en llegar.

Estos dos mecanismos pueden producir una **sincronización global** (indeseada) cuando los paquetes descartados provienen de **distintas conexiones no sincronizadas** entre sí.

### Políticas que influyen en la congestión

**TODO**

### Causas de la congestión

### Mecanismos

Existen 3 mecanismos generales:
* **Pre-asignar recursos** para evitar la congestión $\implies$ Subutilización.
* **Liberar recursos** y controlar la congestión sólo si ocurre $\implies$ ¿A quién perjudicamos?
* Cuando hay problemas, distribuir el impacto en QoS **equitativamente**-

**TODO**.

#### Criterios de Evaluación

La idea es que la red sea utilizada **eficientemente** y de forma **equitativa**. Un buen criterio de evaluación es la **potencia**:
$$Potencia = \frac{throughput}{delay}$$

**TODO**: gráfico

Otro criterio es la **equidad**: se busca que los recursos sean compartidos equitativamente "en las buenas y en las malas".

**Indicador de Equidad de Jain**:

**TODO**

### Teoría de Control

Los algoritmos de control de congestión se pueden clasificar entre:
* **Lazo Abierto**: Se controla en base a una predicción de cómo va a ser el estado del sistema en el futuro.
* **Lazo Cerrado**: El control utiliza mediciones actuales del sistema para tomar decisiones del sistema.
	* **Realimentación Explícita**
		* Los paquetes tienen **marcadores especiales** que indican si el enlace está experimentando congestión.
	* **Realimentación Implícita**
		* Se infiere la congestión a partir de algún indicador existente (timeouts o ACKs repetidos).

#### RED

El algoritmo **RED** (**Random Early Detection**) tiene por objetivo **evitar** la congestión manteniendo el tamaño medio de las colas en niveles "relativamente bajos".
* RED no necesita que los routers mantengan ninguna información del estado de las conexiones.
* RED fue diseñado para trabajar en colaboración con mecanismos de control de congestión en la capa de transporte (TCP).
* Es una técnica de las conocidas como AQM.

El procedimiento es el siguiente:
* Se computa $AvgLen$, el tamaño promedio de la cola a lo largo de una ventana de tiempo (filtro pasa baja).
* Se tienen parámetros $MinThresh$ y $MaxThresh$.
* **TODO**

##### Ajustes de RED

* **TODO**.

#### FRED

**TODO**

## Control de Congestión - Emisor

Para evitar mandar demasiados paquetes, cada host emisor mantiene una **ventana máxima de emisión** (separada de la de control de flujo) $W_c$. Esta ventana se actualiza de la siguiente manera:
* Si se recibe un **ACK**: $W_c \gets W_c + \frac{1}{W_c}$.
* Si se pierde un **ACK**: $W_c \gets \frac{W_c}{2}$

**TODO**: Performance TCP