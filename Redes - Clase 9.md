Institution: [[UBA]]
Course: [[Teoría de las Comunicaciones]]
Type: #source #lecture
Topics: #networking

---

# Tecnologías Inalámbricas

Los medios **inalámbricos** se caracterizan por las siguientes propiedades:
* Es un [[Redes - Clase 2#Medios de Transmisión|medio no guiado]].
	* La intensidad de señal disminuye (más rápidamente) con la distancia.
	* El ruido es más impredecible.
	* En general, las ondas son transmitidas de forma **no direccionada**. Esto causa interferencias con otros transmisores.
		* Es necesario manejar el **acceso compartido**.
	* Las **[[Frecuencia|frecuencias]]** con las que se pueden transmitir están reguladas.
		* Hay bandas del espectro en las se necesitan licencias para transmitir.
		* Hay otras libres/no licenciadas.
	* El medio permite "pinchar" una comunicación fácilmente.

## Bandas no licenciadas

**TODO: Imagen bandas no licenciadas**

* Están sujetas a limitaciones en la potencia de transmisión.
	* Esto limita la distancia y **TODO**.
	* **TODO**

Cuando aceptamos la "no privacidad" de las señales, tenemos 2 problemas comunes.

### Problema de la estación oculta

**TODO: Diagrama**

Supongamos que **$A$ transmite a $B$**, y **$C$ también desea transmitir a $B$**.
* Cuando $C$ detecta el medio, no escuchará la transmisión de $A$ (porque no está en su radio de alcance), y por lo tanto deducirá erróneamente que puede transmitir.
* **TODO**.

### Problema de la estación expuesta

**TODO: Diagrama**

Si **$B$ transmite a $A$**, y **$C$ desea transmitir a $D$**.
* **TODO**

## [[IEEE 802.11]]

El protocolo 802.11 (también conocido como **Wi-Fi** o **Wireless LAN**), es un protocolo de capa física y enlace.

No se puede utilizar el mecanismo **CSMA/CD** porque:
* Se requiere la implementación de una radio **full-duplex** (es muy caro).
* En un medio wireless, no todas las estaciones pueden escucharse entre sí (porque pueden no estar en los radios de transmisión de las otras).

En cambio, 802.11 implementa 2 formas de manejar la contención del canal:

* **DCF**: Controla el acceso compartido de forma automática a través de **CSMA/CA**.
* **PCF**: Se configura manualmente los tiempos/frecuencias de transmisión de cada estación.

**TODO: Diagrama de referencia**

### CSMA/CA

El protocolo de acceso múltiple **CSMA/CA** (Carrier Sense Multiple Access/Collision Avoidance) soluciona los problemas mencionados anteriormente. El procedimiento general es el siguiente:
* Antes de transmitir, las estación **sensan** el estado del medio.
	* Si el canal **no está ocupado**, se realiza una espera adicional llamada **espaciado entre tramas** ($IFS$).
		* Después de transmitir, se espera a rcibir un $ACK$.
		* Si no se recibe el $ACK$ (transcurre el timeout), se asume que se perdió y se retransmite.
	* Si el canal **está ocupado** (o se ocupa durante la espera), se espera hasta el **final de la transacción actual**.
		* La espera continúa hasta que el canal esté libre **durante al menos un $IFS$**.
* Tras finalizar el ciclo (de espera o transmisión), se ejecuta el algoritmo de **[[Redes - Clase 3#Colisiones|Backoff]]**.

### DCF MAC

**DCF** es la implementación de 802.11 del método CSMA/CD.

* Mientras el canal está libre, el nodo decrementa el counter de backoff.
	* Cuando llega a $0$, se transmite el frame.
		* Si la transmisión no es exitosa, 

**TODO: Explicar RTS (ready to send) y CTS (clear to send)**

### Evolución del protocolo

**TODO: Imagen**

# Performance

Los actores que definen la performance de una comunicación son:

* **ISP**: Internet Service Provider.
* **DSLAM**: Digital Subscriber Line Access Multiplexer.
* **BRAS**: Broadband Remote Access Server.
* **HGW**: Home Gateway.
* **PSTN**: Plain Standard Telephone Network.

**TODO: Diagrama**

La performance está limitada por **"el eslabón más débil"**.

## Problemas posibles

* **ADSL**: La mayoría de los planes hogareños que se pueden contratar de un ISP son **ADSL** (Asymmetric Digital Subscriber Line): en este caso, la velocidad de upload es más baja que la de subida (en general es el $20\%$). Esto degrada el servicio para algunas aplicaciones (como las **P2P**).

### Ventana Deslizante

**TODO**

### Control de Congestión

**TODO**
