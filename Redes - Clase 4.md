Institution: [[UBA]]
Course: [[Teoría de las Comunicaciones]]
Type: #source #lecture
Topics: #networking

---

# Nivel de Red

* Tener una **LAN** global es **inviable**.
	* Si todos los clientes de internet compartieran **dominio de broadcast**, los paquetes de broadcast causarían demasiada congestión.
	* Las **VLAN** son una solución parcial, pero no es suficiente.
* **TODO**.

## [[Switch]]

* Un switch es un dispositivo que interconecta *enlaces* para formar redes más grandes.
	* Tiene múltiples entradas y múltiples salidas.
* Cuentan con un **buffer interno**, lo cual permite evitar colisiones: cuando llegan múltiples paquetes al mismo tiempo, se ponen en una cola, y se envían a medida que la red se descongestiona.
	* Esto implica que los switchs segmentan dominios de colisión.
* Los switchs permiten construir redes escalables.
	* Colocar un nuevo host a un switch no (necesariamente) agrega carga a la red: depende del comportamiento del host.

## Tipos de Servicio

Al igual que [[Redes - Clase 2#Tipos de servicio|en el nivel de enlance]], existen distintos servicios que la capa de red puede proveer a las superiores.

### Conmutación sin conexión (datagramas)

En los protocolos sin conexión, la información se parte en paquetes, que se transmiten de forma independiente entre sí.
* Cada paquete puede tomar un camino distinto.
	* Esto implica que pueden llegar desordenados.
* Cada paquete debe tener toda la información necesaria para llegar a su destino, lo cual agrega **overhead** en forma de **headers de control**.
* Los nodos pueden enviar datos tan pronto como esté listo. Si la red
* Un nodo origen no tiene por qué saber si la red es capaz de entregar un paquete.

![[packet-switching.png]]

#### Enrutamiento

Los switchs de la red mantienen una **tabla de forwarding** que indica el **puerto** por el que debe enviarse un paquete para cada **destino**.
* Las tablas son **dinámicas**: pueden cambiar durante su operación, lo cual causa que paquetes que van a un mismo host puedan tomar **distintos caminos**.
* Los **protocolos de enrutamiento** que arman las tablas se ven la clase que viene.

### Conmutación de circuitos virtuales

En estos protocolos, los clientes establecen un **circuito virtual** para enviar los datos.
* Se requiere una **fase inicial** para establecer una conexión y otra **fase final** para terminarla.
	* Esto es un costo inicial para la conexión.
* Una vez establecida, los paquetes se transmiten por un **único camino**, y no requieren mucha información para indicar la conexión a la que pertenecen.
	* Esto hace que el **overhead de transmisión** sea bajo.
* Cuando uno de los nodos de la red falla, se debe establecer una nueva conexión.
	* Esto hace que el **overhead de recuperación ante errores** sea alto.
* Establecer una conexión de antemano permite reservar recursos (como espacio en los buffers) en los switchs.

![[virtual-circuits.png]]

Hay 2 tipos de conexiones:
* **Conexión por solicitud** (SVC): Para cada conexión, los nodos solicitan la formación de un circuito virtual. 
* **Conexión permanente** (PVC): Las conexiones las define y finaliza **el administrador** de la red.

### Source Routing

En algunos protocolos, es posible que el emisor de los paquetes incluya, dentro de los mismos, los nodos por los que desea que se routeen. Esto se denomina **source routing**, y puede ser utilizado tanto en protocolos de **conmutación de paquetes** como en los de **circuitos virtuales**.

## [[Protocolo IP]]

IP es el protocolo de capa de red más utilizado en Internet. Es de carácter:
* **Sin conexión** (basado en datagramas).
* **Best-effort** (no confiable). Los datagramas pueden:
	* Perderse (no se recuperan por el protocolo).
	* Llegar fuera de orden.
	* Llegar duplicados.
	* No tienen cota para el tiempo de entrega.

### Cabecera

Un **datagrama** del protocolo IP, al ser un protocolo sin conexión, tiene una gran cantidad de **información de control**, porque cada paquete debe poder ser **routeado** individualmente. El header incluye:

* **Versión**: v4 o v6, define el formato del resto del header (el siguient es el de v4).
* **Longitud del header**: en palabras de 32 bits, mín 5 y máx 15.
* **Longitud total**: en bytes, máx $2^{16}$.
* Información de **Fragmentación**:
	* Identificador.
	* DF.
	* MF.
	* Desplazamiento del fragmento.
* **TTL (Time To Live)**: cantidad de saltos por los que puede pasar el paquete.
* **Protocol**: el protocolo de la capa superior que está llevando el paquete.
* **Checksum**: del header, no los datos.
* **Dirección fuente/destino**: 32 bits c/u.
* **DiffServ/ECN**: Calidad de servicio/Control explícito de congestión.

![[header-ip.png]]

### Fragmentación

Cada tecnología de red tiene, en el **nivel de enlace**, un **MTU** (**Maximum Transmission Unit**). Cuando un router debe enviar un mensaje más largo que el MTU debe **fragmentarlo** en varios paquetes:
* Se arman múltiples datagramas, llamados **fragmentos**, de a lo sumo **MTU** bytes.
	* Cada fragmento hereda la **misma cabecera** que el datagrama original, excepto por los **campos especiales**: MF (*More Fragments*) y DF (*Don't Fragment*).
		* Todos los fragmentos, excepto el último, tienen 1 en el bit MF.
	* La unidad básica de fragmentación es 8 bytes.
		* Por ender, toda red debe tener un $MTU \geq 68 B$ (60 de header y 8 de datos)
* En el host destino (**no un router**), se **reensamblan** los fragmentos.
	* Los fragmentos comparten un **identificador** que indica el mensaje al que pertenecen.
* La fragmentación tiene una gran **carga computacional**.

![[fragmentación-ip.png]]

### Direcciones IP

* **Direccionamiento Global**: es el que se usa para el tráfico general de internet.
	* Cada host tiene una dirección global **única**.
	* Mide 32 bits $\implies$ $2^{32} \approx 4.300.000.000$ de direcciones distintas.
	* Son **jerárquicas**: una parte indica la **red**, y otra el host dentro de la **red**.
		* Originalmente, esto se identificaba por un esquema de clases:
			* **Clase A** ($0.0.0.0 - 127.255.255.255$): 7 bits para la red, 24 para los hosts.
			* **Clase B** ($128.0.0.0 - 191.255.255.255$): 14 bits para la red, 16 para los hosts.
			* **Clase C** ($192.0.0.0 - 223.255.255.255$): 21 bits para la red, 8 para los hosts.
		* Ahora se utiliza una **máscara de red** para indicar qué parte de la IP indica la red.
* **Direccionamiento Privado**: es lo que se usa para redes IP privadas (cuyos clientes no pueden ser accedidos directamente mediante internet).
	* Las direcciones reservadas para esto son:
		* $10.0.0.0 - 10.255.255.255$ (prefijo $10/8$).
		* $172.16.0.0 - 172.31.255.255$ (prefijo $172.16/12$).
		* $192.168.0.0 - 192.168.255.255$ (prefijo $192.168/16$).

### IP Forwarding

Para transmitir los datagramas de IP, los **routers** siguen la siguiente estrategia:

* Si la IP está en una red **directamente conectada** al router $\implies$ forward al host.
	* Los routers conocen las redes directamente conectadas a ellos.
* Si no está directamente conectada $\implies$ forward a otro router.
* Cada router mantiene una **forwarding table** que indica, para cada red, el puerto por el que se debe enviar un paquete destinado a esa.
	* Si no tiene una entrada para un paquete, lo envía por el **default router**.