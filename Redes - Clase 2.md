Institution: [[UBA]]
Course: [[Teoría de las Comunicaciones]]
Type: #source #lecture
Topics: #networking

---

# Medios de Transmisión

Los medios de transmisión se dividen en 2 categorías:

* **Medios guiados**: Se confina la onda de la señal en un medio físico (en general un cable).
	* *Par trenzado de cobre*: transmite señales de teléfono (y ahora de internet).
	* *Coaxial*:  transmite señales de televisión (y ahora de internet). Tienen cierta resistencia, así que se colocan amplificadores en intervalos regulares.
	* *Red Eléctrica*.
	* Fibra óptica multimodo/monomodo.
* **Medios no guiados**:
	* *Radio*.
	* *WiFi*.
	* *Conexión Satelital*.
	* *Láser*.

## [[Multiplexación]]

**SE TOMA EN EL FINAL**

### Tipos de multiplexación

#### Conmutación de circuitos

En este tipo de redes, cada comunicación se realiza estableciendo un circuito que conecta a ambas partes. En general, estos circuitos deben pasar por tramos con altos niveles de tráfico, donde deben compartir el canal con otros. Hay dos tipos generales de **multiplexación** que se utilizan para dividir el uso del canal:

* **TDM** (*Time Division Multiplexing*): Los usuarios toman turnos (en **[[Round Robin|round robin]]**), donde en cada turno obtienen el **ancho de banda completo** por un período de tiempo acotado. En el caso de la red telefónica, la frecuencia de switching debe ser suficientemente alta como para que los usuarios no perciban problemas en el audio.
	* Solo puede ser utilizado para **datos digitales**.
	* Se puede implementar completamente por **electrónica digital**.

![[tdm.png]]

* **FDM** (*Frequency Division Multiplexing*): El espectro de frecuencias se divie en canales de **ancho de banda acotado** que se asignan a cada usuario. Estos usan el canal **a tiempo completo** y **exclusivamente**.
	* Para utilizarse, requiere **circuitería analógica no trivial**.

![[fdm.png]]


La multiplexación requiere que existan 2 nodos que conocen a todos sus clientes, y que saben el método que utiliza el otro nodo. Esto es infactible en la red de internet, así que se utiliza la **conmutación de paquetes**.

#### Conmutación de paquetes

* **Redes de circuitos virtuales**: Para cada comunicación, se establece un **circuito virtual** que establece una ruta entre ambas partes. Lo vemos en la [[Redes - Clase 3|clase siguiente]].
* **Redes de datagramas**: En este caso, los usuarios dividen las señales en **[[Paquete (Redes)|paquetes]]**, que son *routeados* por la red de la forma más rápida posible (distintos paquetes pueden tomar distintos caminos).

##### Multiplexación Estadística

En entorno de internet no hay uniformidad entre los usuarios de la red: algunos necesitan enviar pocos paquetes en intervalos no regulares (tráfico rafagoso/*bursty*), o tener una comunicación pesada y constante (como el *streaming*).

* Los paquetes de diferentes fuentes **compiten** por el enlace.
* Se **encolan** los paquetes en un **buffer** cuando el enlace no está disponible.
* Dividisón del tiempo, pero **bajo demanda**: en vez de hacer round robin entre las conexiones, donde los clientes reciben una proporción equitativa del canal, los paquetes se atienden [[FCFS]].
* Cuando hay *overflow* del buffer, hay **congestión** en la red.

![[statistical-tdm.png]]

### [[Conversión Analógico-Digital]]

* Parte de la **red de internet** pasa por la **red de telefonía**. Estos segmentos están diseñados para señales analógicas, y [[Redes - Clase 1#Ancho de banda|por ende]], no son buenos para transmitir las ondas cuadradas.
* En cambio, la transmisión de *bits* de las computadoras se convierte en una [[Onda Analógica|onda analógica]] para transmitirse, que en el otro extremo se volverá a convertir en [[Onda Digital|una digital]].
* Existen 2 parámetros importantes en la conversión Analógico$\to$Digital:
	* **Frecuencia de muestreo**: Es la cantidad de **muestras** que se toman de la señal por segundo. Según el **[[Teorema de Muestreo de Nyquist|teorema de muestreo de Nyquist]]**, cuando la señal tiene una frecuencia máxima de $f_m$, alcanza con una frecuencia de muestreo $f_s > 2 f_m$ para poder reconstruirla.
	* **Bit-depth**: Es la cantidad de bits en la que se divide el rango dinámico de la onda. Cada una de las muestras tomadas tendrán este peso.

![[conversor-analógico-digital.png]]

Los dispositivos que digitalizan las señales analógicas se denominan **CODEC** (COder-DECoder).

* Un ejemplo sería el CODEC de PCM, diseñado para la comunicación telefónica.
	* Tiene una frecuencia de $8KHz$, debido a que estudios determinaron que el rango de frecuencias $0Hz-4KHz$ es suficiente para mantener una conversación.
	* Cada muestra es de 8 bits, lo cual implica que un canal de voz requiere una tasa binaria de $8bps \cdot 8KHz = 64Kbps$ para ser transmitido.

### Modulación

La modulación es el proceso de variar alguna/s característica/s de una onda **portadora**, que no tiene información, para introducirle los datos de una señal **modulante**.

#### Portadoras analógicas

A las ondas portadoras analógicas, se les puede variar/*modular*:

* Su **frecuencia** (FM).
* Su amplitud (AM).
* Su fase (PM).

![[am-pm-fm.png]]

##### Moduladora analógica - Portadora analógica

* El caso más famoso es la transmisión de **radio**, que suele utilizar modulación de frecuencia (FM) o de amplitud (AM).
* Esta modulación analógica-analógica se puede implementar utilizando solo electrónica analógica.

![[modulación-analógico-analógico.png]]

#### Moduladora analógica - Portadora digital

El uso más famoso de esto es en la modulación de datos de internet para transmitirse a través de la **red telefónica** (que está diseñada para transimitir señales de voz humana).

Los distintos tipos de modulación tienen otros nombres:

* Desplazamiento de Amplitud (ASK).
* Desplazamiento de Frecuencia (FSK).
* Desplazamiento de Fase (PSK).
* Multinivel.

##### ASK

En *Amplitude-Shift Keying*, los valores binarios se representan con 2 amplitudes diferentes de la portadora:

$$
S(t) =
\begin{cases}
	A\cdot \cos{(2\pi f_c t)} & \text{si } 1 \\
	0 & \text{si } 0
\end{cases}
$$

![[ask.png]]

##### FSK

En *Frequency-Shift Keying*, los valores binarios se representan mediante 2 frecuencias diferentes en la portadora:

$$
S(t) =
\begin{cases}
	A\cdot \cos{(2\pi f_1 t)} & \text{si } 1 \\
	A\cdot \cos{(2\pi f_2 t)} & \text{si } 0
\end{cases}
$$

![[fsk.png]]

##### PSK

En *Phase-Shift Keying*, los valores binarios se representan variando la fase de la portadora:

$$
S(t) =
\begin{cases}
	A\cdot \cos{(2\pi f_c t + \pi)} & \text{si } 1 \\
	A\cdot \cos{(2\pi f_c t + 0)} & \text{si } 0
\end{cases}
$$

![[psk.png]]

##### Multinivel

Podemos lograr una mejor utilización del **ancho de banda** de la señal portadora si cada "variación" representa más de 1 bit:

$$
S(t) =
\begin{cases}
	A \cdot \cos{(2 \pi f_c t + \frac{1}{4} \pi)} & \text{si } 11 \\
	A \cdot \cos{(2 \pi f_c t + \frac{3}{4} \pi)} & \text{si } 10 \\
	A \cdot \cos{(2 \pi f_c t + \frac{5}{4} \pi)} & \text{si } 01 \\
	A \cdot \cos{(2 \pi f_c t + \frac{7}{4} \pi)} & \text{si } 00
\end{cases}
$$

Además, se puede modular 2 parámetros juntos, como en la técnica ASK-PSK.

##### Velocidad de modulación

La **velocidad de modulación** $V_m$ es el número de cambios que se realizan por segundo, medida en *baudios* (símbolos/segundo). Estos símbolos tienen una cantidad de *bits* $N$, por lo que la velocidad de transmisión final es:
$$V_t = V_m \cdot N$$

#### Portadoras digitales

**TODO**

* Moduladora digital - Portadora analógica.
* Moduladora digital - Portadora digital.

##### QAM

# Nivel de Enlace

En el nivel de enlace, abstraemos la capa física como un "caño" **serial** (no hay *desordenamiento*). Este está sujeto a **ruidos** y **fallas**.

A las capas siguientes, se pueden proveer los servicios de:

* **Confiabilidad**: ¿Confiable o no confiable?
* **Control de Error**: ¿Se produjo algún error? ¿Qué hacemos con los errores?
* **Control de Flujo**.

## Framing

En el nivel de enlace, los bits del mensaje se encapsulan en **frames**, y se les agrega **información de control** por medio de *headers*, que preceden al mensaje, y *footers*, que lo suceden. En este contexto, el mensaje es denominado **payload**.

### Largo de frame

¿Cómo se **dividen** los frames en un "tren" de bits? Tenemos varias opciones:

* **Definir un largo fijo**: Se determina una cantidad de bits fija que ocupa cada frame.
	* Si el largo es demasiado grande, los mensajes cortos desperdician mucho espacio.
	* Si el largo es demasiado chico, los mensajes grandes tienen un mayor overhead por la fragmentación (debido al header/footer de cada frame).
* **Largo variable**: Se reserva espacio en el header para especificar el largo total del frame (esto impone un largo máximo, porque el campo debe tener una cantidad de bits fija).
* **Delimitadores**: Se define una secuencia de bits que actúa como delimitador entre los distintos frames.
	* Si la secuencia aparece en el payload, se agregan bits para evitar que esto se interprete como fin de frame, de forma similar a *escaping* del " en una string a través del \\. Luego, el receptor debe descartar estos bits agregados. Esto se conoce como **bit-stuffing**.

### Detección/Corrección de Errores

Los frames pueden contener información adicional que permite **detectar/corregir** ciertos errores. La teoría de la información nos da una cota sobre la cantidad de errores que podemos corregir:

* Tenemos un frame de $n$ bits, al que agregamos $r$ bits de redundancia (utilizados para la detección). El resultado se denomina **codewords**.
* La **[[Distancia de Hamming|distancia de Hamming]]** entre 2 cadenas de bits es la cantidad de posiciones en las cuales difieren.
* Si $d$ es la distancia mínima entre 2 codewords de nuestro esquema:
	* Podemos detectar a lo sumo $d - 1$ errores.
	* Podemos corregir a lo sumo $\left\lfloor \frac{d - 1}{2} \right\rfloor$ errores.

## Tipos de servicio

Los siguientes son los tipos de servicio que se

* Sin **conexión** y sin **reconocimiento**: se envían los datos sin ver si llegan correctamente al destino.
* Sin **conexión** y con **reconocimiento**: se envían los datos, y se verifica que se reciban bien (reenviando si no es el caso). Para asegurar la confiabilidad, las técnicas pueden ser:
	* **Implícitas**: cuando pasa un *timeout* sin recibir ACK, el emisor asume que se perdió el mensaje, y lo reenvía.
	* **Explícitas**: el receptor debe detectar los errores y pedir de nuevo el mensaje. 
* **Orientado a la conexión**: se envían los datos, verificando la recepción, pero además se provee un servicio de conexión estable (**sesión**).

### Servicios sin conexión y con reconocimiento

Estos son los servicios de **transmisión confiable**. En este caso, cuando el receptor recibe un frame, devuelve al receptor (por el mismo canal) un código de **acknowledge** (ACK).

#### Stop and Wait

En este caso, el emisor **espera** un ACK después de cada frame enviado. Si pasa un **timeout**, reenvía el frame.
* El método es confiable, pero es vulnerable al **problema de la reencarnación**:
	* El emisor envía un mensaje, y el receptor lo recibe, pero pasa el timeout antes de que el ACK llegue al emisor.
	* El emisor retransmite el mensaje, y recibe el ACK. Como envió 2 mensajes, pero recibió 1 solo ACK, debe esperar a otro.
	* Esto puede repetirse infinitamente.
* Para evitarlo, es necesario incluir información sobre la posición de los mensajes en la secuencia. En este caso, alcanza con 1 solo bit.
* Es sumamente **ineficiente**: sólo se puede enviar $1$ mensaje cada $RTT$ (hay que esperar a que el mensaje llegue y vuelva el ACK), por lo que el canal de comunicación pasa la mayor parte del tiempo vacío.

![[diagrama-stop-and-wait.png]]

#### Sliding Window

En vez de enviar un sólo frame y esperar un ACK, el emisor envía una **ventana** de frames (de longitud $SWS$), esperando el ACK de cada uno. Los mensajes ACK ahora tienen un valor que indica la posición en la secuencia. Estos protocolos se dividen en 2 tipos:

* **Acumulativos/GoBackN**: el ACK sólo indica el último mensaje de la secuencia recibido correctamente.
	* Si el receptor recibió el mensaje #2, y luego recibe el #4, no enviará más mensajes ACK hasta recibir el #3, lo cual indicará (después de un timeout).
	* La $RWS$ es de 1: el receptor descarta todos los mensajes que no sean el que corresponde después del último correctamente recibido.
		* Esto implica que no se necesita un **buffer de repeción**.
* **Selectivos**: se envía un ACK para cada mensaje recibido (aún cuando faltan algunos anteriores).
	* El mensaje de acknowledge contiene tanto el número recibido como el último punto de la secuencia hasta el que se tienen todos los mensajes. Si estos difieren, el emisor sabe que hubo un problema, y envía los faltantes de nuevo.
	* Se tiene $RWS = SWS$.
	* **TODO: Piggybacking**

![[diagrama-sliding-window.png]]

Para distinguir reencarnaciones, se requiere que:
$$\#\text{frames unívocamente identificables} \geq SWS + RWS$$

## Eficiencia de Protocolo

La **eficiencia de protocolo** es una medida de cuánto tiempo se pasa transmitiendo con respecto al tiempo bloqueado esperando. Está definido como:
$$\eta_{proto} = \frac{T_{tx}(V)}{RTT(F)}$$
Donde:

* $T_{tx}(V)$ es el tiempo de transmisión de una ventana de emisor. Si se tiene $V = SWS = V_{tx}\frac{RTT}{|frame|}$ (ventana de emisión óptima), hay eficiencia $\eta_{proto} = 1$ (si no hay errores, el canal siempre está lleno).
* $RTT(F)$ es el round-trip time de un frame, definido como $Delay(F_E) + Delay(F_R)$ (el delay del frame enviado más el delay del frame de respuesta).