
# Medios compartidos

* En las redes que componen internet, se suele tener muchos más usuarios que cables distintos para comunicarse entre sí.
* Podríamos resolver esto utilizando [[Multiplexación|multiplexación]], pero este método requiere un nodo central, y no escala bien.
* Buscamos un protocolo que permita a los clientes comunicarse entre sí, controlando el acceso de forma **descentralizada**.
	* Cuando se da un conflicto, este puede ser **manejado** o **aceptado**.

## [[Protocolos MAC]]

* Los protocolos de control de acceso al medio (MAC) buscan maximizar la cantidad promedio de **éxitos** en los intentos de comunicación (sin necesidad de un nodo de comunicación central).
* Implementan **[[Redes - Clase 2#Multiplexación Estadística|multiplexación estadística]]**, donde el acceso a la red se distribuye en base a la demanda.
* Intentan asegurar **igualdad de oportunidades** (**average fairness**) entre los nodos competidores.

### [[CSMA-CD]]

* El protocolo *Carrier Sense Collision Detection* es el utilizado en el estándar **[[Ethernet 802.3]]**. Provee un servicio **[[Redes - Clase 2#Tipos de servicio|sin conexión y sin ACK]]**, pero evita colisiones. Para lograrlo indica que, cuando un host quiere enviar datos, **sensa el medio**:
	* Si el medio está **libre**, los envía.
	* Si el medio está **ocupado**:
		* **$1$-persistente**: espera a que se libere, y transmite (es el caso de Ethernet IEEE 802.3).
		* **$p$-persistente**: espera a que se libere, y transmite con probabilidad $p$. Esto previene colisiones.
* Es **half-duplex**: los hosts pueden estar enviando datos, o recibiéndolos, pero no ambas al mismo tiempo.
* Habiendo **delay de propagación**, es posible que múltiples hosts detecten el canal vacío, y envíen un mensaje al mismo tiempo. En tal caso, se produce una **colisión**.
* Las colisiones se pueden detectar, porque las ondas enviadas **interfieren** entre sí, y resulta (muy) probable que se alcance una **potencia mayor** a la que se transmite normalmente.
	* La detección **no es inmediata**, porque la señal afectada debe llegar a los clientes.
	* Este tiempo de llegada está **acotado por el $RTT$**, así que el emisor puede esperar ese tiempo para ver si el mensaje se envió correctamente.
	* En vez de esperar el tiempo **idle** (lo cual requiere un clock y desperdicia tiempo), el emisor puede aprovechar y enviar un **frame más largo**.
		* Para esto, el **largo mínimo** de un frame debe ser $2C_{vol}$.

![[diagrama-estado-csma-cd.png]]

### Colisiones

![[colisión-csma-cd.png]]

* Cuando se da una **colisión**, se debe esperar un tiempo, y reenviar el paquete. Existen distintos métodos para determinar el **tiempo de espera**:
	* Retransmitir **inmediatamente**: puede llevar a loops infinitos, donde ambos emisores envían mensajes que colisionan.
	* Retransmitir luego de un **tiempo fijo**: al igual que el anterior, puede llevar a loops infinitos.
	* Retransmitir luego de un tiempo aleatorio: los clientes esperan un tiempo aleatorio después de la colisión, lo cual permite que intenten transmitir en momentos distintos y, por ende, no se dé otra colisión.
		* **Exponential BackOff** es un esquema donde el tiempo de retransmisión escala con la cantidad de intentos de transmisión:
			* En el intento $k$, los clientes elijen un $slot$ entre $0$ y $2^k - 1$ y esperan $slot \cdot RTT$ para retransmitir.

## LAN

* Una **LAN** es una red donde todas las computadoras están conectadasa un mismo enlace. Cada **cliente** tiene una **dirección**, y recibe (los otros también los lee, pero los ignora) sólo los paquetes que:
	* Tienen su dirección como destino.
	* Tienen como destino una dirección de **multicast** a la que está suscripta.
	* Tienen como destino la dirección de **broadcast** (**FF:FF:FF:FF:FF:FF**).
	* Todos los paquetes, si está en **modo promiscuo**.
* Todos los hosts comparten un mismo **esquema de direccionamiento** y, por ende, un mismo **dominio de broadcast**.
	* Un dominio de broadcast es un conjunto de clientes que reciben los broadcasts de los demás.
* Los **hubs** son dispositivos que conectan varios cables entre sí, como si estuvieran soldados. Esto nos permite combinar 2 redes LANs, creando una más grande.
	* Todas las partes conectadas a un hub comparten **dominio de colisión**

### LANs Extendidas

Distintas LANs se pueden conectar entre sí, compartiendo un dominio de broadcast, pero no un **dominio de colisión**: éste es el conjunto de clientes cuyos mensajes colisionan si se mandan concurrentemente.

* Las LANs extendidas se forman conectando distintas LANs con **switchs/bridges**:
	* Los **switchs** son dispositivos que, a diferencia de los hubs, cuentan con un buffer de paquetes
	* Esto permite almacenar mensajes de varias LANs en caso de que se envíen al mismo tiempo, evitando las colisiones.
* Además de segmentar el dominio de broadcasts, los **switchs/bridges** permiten conectar redes que usan distintas tecnologías (como WiFi y Ethernet).
* A medida que aumenta la cantidad de clientes, el esquema de LAN extendida no escala bien, porque los broadcasts (que deben llegar a todos los hosts) generan demasiada congestión.

**PREGUNTA DE FINAL**: Explique diferencia entre **dominio de broadcast** y **dominio de colisión**.

![[dominio-broadcast-colisión.png]]

### [[Learning Bridges]]

Este es un protocolo que busca mejorar la **escalabilidad**, es decir, funcionar bien a medida que crece la cantidad de clientes interconectados. Se busca minimizar las **colisiones innecesarias**.

* No reenviamos paquetes innecesarios: si dos redes están conectadas a puertos distintos, y se envía un paquete entre 2 hosts de una misma red, solo se debería enviar por el puerto correspondiente.
* Se mantiene una **Tabla de reenvío**.
	* Las entradas se **aprenden** a partir de las direcciones de origen de los paquetes recibidos.
		* Si se recibe un paquete **emitido** por **A**:
			* Cuando se recibe un paquete de un puerto dado, aprendemos que la máquina que lo envió está conectada a ese puerto.
			* Esta información tiene un **TTL** (Time To Live), es decir, expira después de un tiempo, para poder adaptarse a **cambios en la topología**.
		* Cuando recibimos un paquete **destinado** a **B**:
			* Si tenemos una **entrada** que nos indica el puerto correspondiente, lo enviamos por ahí.
			* Si no tenemos, se envía a todos los puertos (se **inunda**).
* La tabla es una **heurística**: no necesita ser perfecta.
* Los broadcasts de los clientes siempre se envían.

Este esquema simple se rompe cuando la topología tiene **ciclos**. Consideremos la siguiente red:

![[learning-bridges-ciclos.png]]

* En este caso, ambos bridges reciben un paquete y, al no saber dónde está el receptor, lo envían por broadcast.
* Como ambos están conectados a la LAN 2, cada uno recibe el paquete del otro, por lo cual asumen que el emisor proviene de ahí (cuando en realidad es de la LAN 1).

### [[Spanning Tree Protocol]]

El protocolo STP arregla el problema de los ciclos anterior. Lo hace armando un **[[Árbol Generador|árbol generador]] lógico** del **grafo** de la red (la topología física se mantiene igual), asegurando que haya un **camino único** entre cada par de nodos. Esto se logra a través del siguiente algoritmo  distribuido:

**La idea general**: Cada switch envía paquetes (BPDUs) a sus vecinos, propagando información acerca de la topología de la LAN, de manera periódica. El mecanismo es el siguiente:
* Se elige un switch **root**.
* Cada switch aprende las distancias al **root** de todos sus vecinos.
	* Utiliza esa información para determinar el **switch de distancia mínima** al root, la cual queda designada como **interfaz única** hacia el este.
* Los **BPDUs** están conformados por:
	* El *id* de quien envía el mensaje.
	* El *id* del root según el que está enviando el mensaje.
	* La distancia, en saltos, desde el emisor hasta su root designado.
* La información de un switch se **actualiza** cuando:
	* Se identifica un BPDU con menor *root id*.
	* Se identifica un BPDU con igual *root id* pero menor distancia a ese.
	* El *root id* y la distancia son las mismas, pero el *id* del switch es menor.

Después de correrlo, cada puerto de cada switch tiene un **tag**, que puede ser:
* **Root port**: el puerto del switch con menor distancia al root.
* **Designated port**: son los puertos designados a conectar una red al root.
* **Blocked port**: ningún mensaje se envía por este port.

![[stp.png]]

El puerto por el que se debe enviar cada mensaje se determina a través de **learning bridges**, excepto cuando no se tiene una entrada: en ese caso, no se broadcastea por los puertos bloqueados (lo cual evita ciclos).

## VLAN

* En las LANs extendidas, los broadcast no escalan: empeoran mucho el **goodput** (proporción de transmisiones **existosas**).
* Las VLANs permiten particionar **de manera lógica** una LAN física en varias LANs virtuales.
	* Una parte de la dirección pasa a ser el tag de la red VLAN a la que pertenece.
	* Los broadcasts se envían solo dentro de la VLAN correspondiente: se particiona el **dominio de broadcast**.