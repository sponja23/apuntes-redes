Institution: [[UBA]]
Course: [[Teoría de las Comunicaciones]]
Type: #source #lecture
Topics: #networking

---

# Introducción

* **Conmutación de circuitos**: Hay un (o pocos) cable compartido que puede ser usado para comunicarse entre 2 usuarios (o redes). Un usuario debe llamar a una **central de conmutación** para pedir que establezcan una conexión directa con otro.
* **Conmutación de paquetes** (*packet switching*): Los mensajes entre usuarios se dividen en **[[Paquete (Redes)|paquetes]]**, de forma tal que una misma línea puede contener la información de varias conexiones.
	* Esto, junto con la **descentralización de las redes**, se desarrolló con propósitos militares: la tolerancia a fallas era importante en la guerra fría.

## Arquitectura de Redes

### [[Modelo OSI|Modelo OSI-ISO]]

A mediados de los 80, había múltiples redes independientes que tenían sus propios protocolos. En 1983 se escribió el documento *The Basic Reference Model for Open Systems Interconnection*, definiendo el **modelo OSI**.

La comunicación se divide en 7 capaz de abstracción, donde cada una expone una serie de funcionalidades a la superior, y consume otra de la inferior.

### [[TCP-IP|Modelo TCP/IP]]

La mayoría de las comunicaciones en internet se dan a través de otro modelo: un subconjunto del modelo OSI llamado **TCP/IP**. Este colapsa algunas de las capas del modelo anterior.

![[osi-vs-tcpip.png]]

# [[Capa Física (Modelo OSI)|Capa Física]]

Esta es la capa más baja del modelo OSI. El modelo general puede ilustrarse de la siguiente manera:

![[diagrama-capa-física.png]]

* Una **fuente de información** envía algún mensaje a través de un **transmisor**.
* Este transmisor envía una señal a través del **canal**, que tiene cierto nivel de **ruido**.
* El **receptor** convierte la señal recibida en un mensaje que puede leer el **destino**.

A este modelo simple se le pueden agregar mecanismos de **corrección de señal/mensaje**:

![[diagrama-capa-física-corrección.png]]

Sin embargo, la detección y corrección de mensajes se suele realizar en la siguiente capa, **[[Capa de Enlace (Modelo OSI)|la de enlace]]**.

## Señales

Las **señales** son la "unidad" de transmisión de la capa de red. Se trata de funciones que, en cada instante del tiempo, tiene un valor de **[[Amplitud|amplitud]]**. Cuando la señal es periódica, tiene una **[[Frecuencia|frecuencia]]**.

Las señales pueden ser:

* **[[Onda Analógica|Analógicas]]**: aquellas que cuentan con un rango de valores de amplitud [[denso]] y donde, en general, cada intervalo contiene infinitos valores distintos.

![[señal-analógica.png]]

* **[[Onda Digital|Digitales]]**: estas tienen un rango de valores discreto, y están formadas por intervalos en los que la frecuencia no cambia.

![[señal-digital.png]]

Como las computadoras solo pueden tratar en valores digitales, las ondas analógicas deben ser **[[Conversión Analógico-Digital|discretizadas]]**. Esto implica **samplearlas** con una **frecuencia de sampleo**, y definir un **rango de valores** que se mapean al **rango dinámico** de la señal (**bit-depth**).

![[conversor-analógico-digital.png]]

### Fundamentos físicos

* Las señales se transmiten como **[[Onda Electromagnética|ondas electromagnéticas]]**, variaciones en el campo **[[Campo Eléctrico|eléctrico]]** y el **[[Campo Magnético|magnético]]**.
* Cada **frente de onda** se propaga a una velocidad particular. Esta alcanza su [[Velocidad de la Luz|velocidad máxima]] $c$ en el vacío, pero en general es menor, afectada por el medio de propagación.
* Las ondas son periódicas. Como son físicas, cada ciclo ocupa una distancia espacial, llamada **[[Longitud de Onda|longitud de onda]]** ($\lambda$).	$$\lambda = v \cdot T = \frac{v}{f}$$

### Señales periódicas

La forma general de una onda sinusoidal es:

$$s(t) = A \cdot \sin{(2\pi \cdot f \cdot t + \Phi)}$$
Donde:

* $A$ es la amplitud.
* $f$ es la frecuencia (temporal, la angular es $w = 2\pi f$).
	* Es medida en *Hertz* ($Hz$).
	* Está inversamente relacionada al período $T = \frac{1}{f}$.
* $\Phi$ es la fase.

![[señal-sinusoidal.png]]

#### [[Serie de Fourier]]

Cualquier función periódica $f(t)$ de período $T$ puede expresarse por la siguiente serie, llamada **serie trigonométrica de fourier**:
$$f(t) = \frac{a_0}{2} + \sum_{n = 1}^\infty a_n \cos{(n\omega_0 \cdot t) + b_n\sin{(n\omega_0 \cdot t)}}$$

Donde:
* $w_0 = \frac{2\pi}{T}$ es la **frecuencia fundamental** de la onda.
* Los coeficientes $a_n$ y $b_n$ son los coeficientes de Fourier.
* Los distintos términos se denominan los **armónicos**.

Es decir, cada función periódica es una [[combinación lineal]] de una serie infinita de senoides.

![[fourier-step.png]]

# Teoría de la Información

Sea $E$ un suceso de probabilidad $P(E)$. Cuando sucede $E$, decimos que recibimos:
$$I(E) = \log{\frac{1}{P(E)}}$$
Cuando el logaritmo está en base 2, esa unidad se llama **[[Bit|bit]]**. La definición de esto es *la cantidad de información obtenida al especificar una de dos posibles alternativas igualmente problemas*.

## Fuente de memoria nula

Supongamos que se tiene una fuente que emite una secuencia de símbolos de un alfabeto (finito y fijo) $S = \{s_1, s_2, ..., s_n\}$, llamada **mensaje**. Una **fuente de memoria nula** es una donde la emisión de cada símbolo es estadísticamente independiente de las demás, y tienen probabilidades fijas $P(s_1), P(s_2), ..., P(s_n)$.

La **cantidad media de información** o **[[Entropía|entropía]]** de una fuente de memoria nula está definida como:
$$\sum_{s \in S} P(s) \cdot I(s)$$
Si lo medimos en bits, se vuelve:
$$H(S) = \sum_{s \in S} P(s) \cdot \log_2{\frac{1}{P(s)}} bits$$

Esto se puede interpretar como:

* El **valor medio ponderado** de la **cantidad de información** del conjunto de mensajes posibles.
* Una medida de la **incertidumbre promedio** de una [[Variable Aleatoria|variable aleatoria]].
* La cantidad de información obtenida en promedio al observar un nuevo símbolo.

Propiedades:
* $H(S) \geq 0$, y $H(S) = 0 \iff \text{ uno de los símbolos tiene probabilidad uno y los demás 0}$
* $H(S)$ es máxima cuando los símolos son **equiprobables**: si hay $n$, y cada uno tiene probabilidad $\frac{1}{n}$, se tiene $H(S) = \log_2{n}$.

La extensión de $n$-ésimo orden de una fuente es una en la que los símbolos son las $n$-uplas de $\underbrace{S \times S \times \dots \times S}_n = S^n$, y la probabilidad de cada elemento es el producto de sus partes.

## Codificación

La **codificación** es un proceso que establece una correspondencia entre los símbolos de una fuente y los del alfabeto de un **código**. Buscamos lograr una representación **eficiente** de la información.

### Propiedades

* Un código es **unívocamente decodificable** cuando cualquier secuencia de elementos de su alfabeto tiene a lo sumo 1 significado (es decir, no puede haber un **mensaje ambigüo**).
* Un código es **instantáneo** cuando ninguna de sus palabras es un **prefijo** de otra de longitud mayor.
	* Esto permite que un decodificador pueda determinar el significado de una secuencia inmediatamente después de recibir su último símbolo, ya que no necesita esperar a otros símbolos para ver si es otro más largo.

### Eficiencia

La **longitud media** de la codificación para una fuente dada, donde cada símbolo $s$ tiene una codificación de longitud $L(s)$ y una probabilidad de $P(s)$, está definida como:
$$L = \sum_{s \in S} L(s) \cdot P(s)$$
Si el alfabeto del código es de $r$ elementos, se llama a $\log{r}$ la cantidad promedio de **información máxima** de un símbolo de código. Se tiene:
$$L \log{r} \geq H(S)$$
La **eficiencia** del código para la fuente $S$ está definida como:
$$h = \frac{H(S)}{L \log{r}}$$
Que tiene un valor máximo de 1.

### Codificador óptimo

Un **codificador óptimo** para una fuente $S$ es uno donde la eficiencia es igual a la entropía $H(S)$, y por ende usa el **menor número posible** de bits para codificar los mensajes. Una codificación óptima se puede construir para un mensaje utilizando la [[Codificación de Huffman]].

## Medios de Transmisión

En los medios de transimisión reales, el mensaje recibido en el destino **puede ser distinto** del enviado inicialmente, a causa de:

* Atenuación y distorsión.
* Distorsión de retardo.
* Ruido.

### Atenuación

La intensidad de una señal disminuye con la distancia, a una tasa que depende del medio de transmisión. Para no tener errores, la intensidad debe ser suficientemente grande para detectar la imagen, y suficientemente mayor que el ruido para evitar errores.

### Distorsión de retardo

Solo se da en medios **guiados** (cables, en vez del aire). Como la velocidad de propagación depende de la frecuencia, los componentes de frecuencia llegan al receptor en distintos instantes, creando desplazamientos de fase.

### Ruido

El ruido son señales adicionales que comparten el canal con la señal enviada. Tiene varias fuentes posibles:

* **Ruido Térmico**: por agitación térmica de los electrones, y es proporcional a la temperatura (en Kelvin). 
* **Ruido por Intermodulación**.
* **Ruido por Diafonía**.
* **Ruido Impulsivo**: impulsos irregulares o picos.

![[señal-digital-ruido-impulsivo.png]]

#### Ancho de banda

* Cada **medio de transmisión** tiene una **frecuencia central** $f_c$. Aquellos componentes de frecuencia que se alejan de la central se ven **atenuados**.
* La frecuencia **de corte** es donde se produce una atenuación de $3dB$, y la distancia entre la central y la de corte se denomina **[[Ancho de banda|ancho de banda]]**, medida en $Hz$. 
	* Esto es una medida del rango de frecuencias que un medio puede transmitir sin demasiado ruido. 
* Como las ondas cuadradas tienen **infinitos** componentes de frecuencia (**riqueza frecuencial**), ningún ancho de banda es suficiente para no degradarlas. Es por esta razón que no es deseable transmitir estas señales a largas distancias (se convierten en **señales analógicas**).

### Delay

* $T_{prop}$: retardo de propagación. El tiempo que tarda cada bit en transimitirse físicamente por el medio.
* $T_{tx}$: retardo de transmisión. Es $\frac{\text{tamaño trama}}{\text{velocidad de transmisión}}$
* $T_{encol}$: retardo de encolamiento. Depende de la congestión.
* $T_{proc}$: retardo de procesamiento. En general es despreciable.
$$Delay = T_{prop} + T_{tx} + T_{encol} + T_{proc}$$
En la práctica, despreciamos los retardos de encolamiento y procesamiento, así que:
$$Delay = T_{tx}(n) + T_{prop}$$
Donde $T_{tx}(n) = \frac{n}{V_{tx}}$ es el tiempo de transmisión de $n$ bits.

La **capacidad de volumen** es la cantidad de bits que entran en el medio desde que se envía el primer bit hasta que éste llega al receptor. Está definida como:
$$C_{vol} = Delay \cdot V_{tx}$$

## Capacidad de Canal

La teoría de la información cuenta con 2 teoremas que limitan la **capacidad de canal**: la cantidad de bits por segundo que puede transmitir.

* **Capacidad máxima sin ruido**, llamada **ancho de banda de Nyquist**.
	* Si los símbolos son de **2 niveles** ($0$ o $1$),
$$C = 2B$$
	 Siendo $B$ el **ancho de banda** del canal.
   * Si los símbolos son de **$M$ niveles**,
$$C = 2B \cdot log_2{M}$$

* **Capacidad máxima con ruido**, llamada **[[Teorema de Shannon-Hartley|capacidad de shannon]]**, dada por
$$C_{máx} = B \cdot \log_2{\left(1+\frac{S}{N}\right)}$$
Donde $\frac{S}{N}$, a veces llamada $SNR$, es la [[SNR|relación señal a ruido]], es decir, la relación entre la potencia de la señal y la del ruido.